dev:
	yarn run dev

build:
	yarn run build

lint:
	yarn run lint

format:
	yarn run format

clean:
	rm -rf .next/ dist/
