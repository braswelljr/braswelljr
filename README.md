# `braswelljr` 👨‍🚒

I am [`braswelljr`](https://braswelljr.vercel.app/) a Software Developer based in Kumasi, Ghana. I'm a `Fullstack Web Developer` who mostly runs on `JAVASCRIPT` and also opts into a little bit of designing mobile application `UIs`.

I like to work with the `Frontend` of most applications.

- 🌱 I’m currently learning **Golang**

- 👯 I’m looking to collaborate on [Vue.js](https://vuejs.org/), [Tailwindcss](https://tailwindcss.com), [Zustand](https://github.com/pmndrs/zustand)


## Technical Skills

### Language and tools :

I work with

- JAVASCRIPT --> [ [React](https://reactjs.org/) / [Vue](https://vuejs.org/) / [Node.js](https://nodejs.org/) / [TypeScript](https://www.typescriptlang.org/) / [React Native](https://reactnative.dev/) ]
 
- CSS --> [ [SASS-SCSS](https://sass-lang.com/) / [Tailwindcss](https://tailwindcss.com/) / [Bootstrap](https://getbootstrap.com/) ]
 
- UI / UX --> [ [FIGMA](https://www.figma.com/) / [ADOBE XD](https://www.adobe.com/products/xd.html) ]
 
- PHP [ [Laravel](https://laravel.com/) ]

- [GOLANG](https://go.dev/)

- HTML

<p align="left"> <a href="https://www.chartjs.org" target="_blank"> <img src="https://www.chartjs.org/media/logo-title.svg" alt="chartjs" width="40" height="40"/> </a> <a href="https://www.w3schools.com/css/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> </a> <a href="https://www.figma.com/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/figma/figma-icon.svg" alt="figma" width="40" height="40"/> </a> <a href="https://firebase.google.com/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/firebase/firebase-icon.svg" alt="firebase" width="40" height="40"/> </a> <a href="https://flutter.dev" target="_blank"> <img src="https://www.vectorlogo.zone/logos/flutterio/flutterio-icon.svg" alt="flutter" width="40" height="40"/> </a> <a href="https://git-scm.com/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a> <a href="https://www.w3.org/html/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a> <a href="https://jestjs.io" target="_blank"> <img src="https://www.vectorlogo.zone/logos/jestjsio/jestjsio-icon.svg" alt="jest" width="40" height="40"/> </a> <a href="https://www.mongodb.com/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mongodb/mongodb-original-wordmark.svg" alt="mongodb" width="40" height="40"/> </a> <a href="https://nextjs.org/" target="_blank"> <img src="https://cdn.worldvectorlogo.com/logos/nextjs-3.svg" alt="nextjs" width="40" height="40"/> </a> <a href="https://reactjs.org/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original-wordmark.svg" alt="react" width="40" height="40"/> </a> <a href="https://reactnative.dev/" target="_blank"> <img src="https://reactnative.dev/img/header_logo.svg" alt="reactnative" width="40" height="40"/> </a> <a href="https://sass-lang.com" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/sass/sass-original.svg" alt="sass" width="40" height="40"/> </a> <a href="https://tailwindcss.com/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/tailwindcss/tailwindcss-icon.svg" alt="tailwind" width="40" height="40"/> </a> <a href="https://www.typescriptlang.org/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/typescript/typescript-original.svg" alt="typescript" width="40" height="40"/> </a> <a href="https://vuejs.org/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vuejs/vuejs-original-wordmark.svg" alt="vuejs" width="40" height="40"/> </a> <a href="https://www.adobe.com/products/xd.html" target="_blank"> <img src="https://cdn.worldvectorlogo.com/logos/adobe-xd.svg" alt="xd" width="40" height="40"/> </a> </p>


<h3 align="left">Connect with me:</h3>
<p align="left">
<a href="https://dev.to/brakez_ken" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/dev-dot-to.svg" alt="brakez_ken" height="30" width="40" /></a>
<a href="https://twitter.com/brakez_ken" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/twitter.svg" alt="brakez_ken" height="30" width="40" /></a>
<a href="https://instagram.com/braswell_jr" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/instagram.svg" alt="braswell_jr" height="30" width="40" /></a>
</p>


![braswelljr](https://github-readme-stats.vercel.app/api/top-langs?username=braswelljr&show_icons=true&locale=en&theme=dracula&layout=compact)

<!-- ![braswelljr](https://github-readme-streak-stats.herokuapp.com/?user=braswelljr&) -->
